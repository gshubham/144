/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

uint16_t identificationNumber = 0; /* to identify sent packets uniquely */

#define MAX_TTL 65


bool isNatEnabled(struct sr_instance* sr) {
  return (sr->nat != NULL);
}

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
    
    /* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: handle_arpreq(struct sr_instance *sr, struct sr_arpreq* currentReq)
 * Scope:  Global
 *
 * This function is called every second by arpcache_sweepreqs(). If it has been more than
 * one second for the last instance (of the current request) sent, it checks whether we've exceeded
 * the number of requests (5). If that's the case, an ICMP Destination Host Unreachable reply to all the 
 * requesting hosts and destroys the request. Else, it sends an ARP request to the fetch the MAC Address
 * of the next destination
 *---------------------------------------------------------------------*/
void handle_arpreq(struct sr_instance *sr, struct sr_arpreq* currentReq) {
    if(difftime(time(NULL), currentReq->sent) > 1.0) { /* been more than one second */
        if(currentReq->times_sent >= 5) { /* more than 5 times */
            struct sr_packet* currPacket = currentReq->packets; 
            while(currPacket != NULL) {
              sendDestinationHostUnreachable(sr, currPacket->buf); /* send unreachable ICMP replies*/
              currPacket = currPacket->next;
            }
            sr_arpreq_destroy(&(sr->cache), currentReq); /* destroy */
        }
        else {
            sendARPRequest(sr, currentReq->ip); /* send an ARP request to the current IP */
            currentReq->sent = time(NULL);
            currentReq->times_sent++;
        }
    }
}

/*---------------------------------------------------------------------
 * Method: getIPHeader(uint8_t* packet)
 * Scope:  Global
 *
 * Returns a pointer to the IP header in the Ethernet Frame.
 *---------------------------------------------------------------------*/
sr_ip_hdr_t* getIPHeader (uint8_t* packet) {
  return (sr_ip_hdr_t *)((char*)packet + sizeof(sr_ethernet_hdr_t));
}

/*---------------------------------------------------------------------
 * Method: checkifValid (uint8_t* packet, unsigned int len)
 * Scope:  Global
 *
 * This function checks if the IP Packet is actually big enough to hold an IP Header, and also verifies the checksum of the IP header. 
 * Returns 1 if valid, else returns 0.
 *---------------------------------------------------------------------*/
int checkifValid (uint8_t *packet, unsigned int len) {
  sr_ip_hdr_t* iphdr = getIPHeader(packet);
  uint16_t verifyCheckSumValue = cksum(iphdr, sizeof(sr_ip_hdr_t));
  verifyCheckSumValue = ~verifyCheckSumValue;
  if(verifyCheckSumValue == 0 && ((len - sizeof(sr_ethernet_hdr_t)) >= iphdr->ip_hl)) {
    return 1;
  }
  else {
    return 0;
  }
}


/*---------------------------------------------------------------------
 * Method: checkListOfInterfaces(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function checks if the Destination MAC Address is one of the router MAC Addresses. Returns 1 if it is, else returns 0.
 *---------------------------------------------------------------------*/
int checkListOfInterfaces(struct sr_instance* sr, uint8_t* packet) {
  struct sr_if* currentInterface = sr->if_list;
  size_t numBytes = ETHER_ADDR_LEN;
  sr_ethernet_hdr_t* etherNetHeader = (sr_ethernet_hdr_t*)packet;
  while(currentInterface != NULL) {
    if(memcmp(currentInterface->addr,etherNetHeader->ether_dhost, numBytes) == 0) {
      return 1; 
    }
    currentInterface = currentInterface->next;
  }
  return 0;
}

/*---------------------------------------------------------------------
 * Method: checkListOfIPs(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function checks if the Destination IP is one of the router IPs. Returns 1 if it is, else returns 0.
 *---------------------------------------------------------------------*/
int checkListOfIPs(struct sr_instance* sr, uint8_t* packet) {
  struct sr_if* currentInterface = sr->if_list;
  sr_ip_hdr_t *iphdr = getIPHeader(packet);
  while(currentInterface != NULL) { 
    if(currentInterface->ip == iphdr->ip_dst) { /* found IP match */
      return 1; 
    }
    currentInterface = currentInterface->next;
  }
  return 0;
}
/*---------------------------------------------------------------------
 * Method: findPrefixLength(uint32_t maskAddress)
 * Scope:  Global
 *
 * This function returns the number of "ON" bits in the mask Address (from the routing table) supplied.
 *---------------------------------------------------------------------*/
uint32_t findPrefixLength(uint32_t maskAddress) {
  int currentPrefixLength = 0;
  for(currentPrefixLength = 0; currentPrefixLength < 32; currentPrefixLength++) {
    uint32_t currentValue = maskAddress & (1 << currentPrefixLength);
    if(currentValue) break;
  }
  return (32 - currentPrefixLength);
}

/*---------------------------------------------------------------------
 * Method: runAlgorithmLongestPrefixMatch(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function takes in an entry in the routing table, and if there is a match,
 * returns the length of the match.
 *---------------------------------------------------------------------*/
int runAlgorithmLongestPrefixMatch(struct sr_rt* route, uint32_t destinationIP) {
  uint32_t valueOne = ntohl(destinationIP) & ntohl(route->mask.s_addr);
  uint32_t valueTwo = ntohl(route->dest.s_addr) & ntohl(route->mask.s_addr);
  if((valueOne == valueTwo)) {
    return findPrefixLength(ntohl(route->mask.s_addr));
  }
  return -1;
}


/*---------------------------------------------------------------------
 * Method: longestPrefixMatch(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function goes through all entries in the routing table and finds the longest prefix match. It returns a pointer
 * to that entry. In case it cannot find an entry that matches, it returns NULL.
 *---------------------------------------------------------------------*/
struct sr_rt* longestPrefixMatch(struct sr_instance* sr, uint8_t* packet) {
  struct sr_rt* currentRoute = sr->routing_table;
  sr_ip_hdr_t *iphdr = getIPHeader(packet);
  int maxPrefixLength = -1;
  struct sr_rt* maxRoute = NULL;
  while(currentRoute != NULL) {
    int currentPrefixLength = runAlgorithmLongestPrefixMatch(currentRoute, iphdr->ip_dst); /* find current prefix match */
    if(currentPrefixLength != -1) { /* if exists */
      if(currentPrefixLength > maxPrefixLength) { /* check if big enough to be max */
        maxPrefixLength = currentPrefixLength;
        maxRoute = currentRoute;
      }
    }
    currentRoute = currentRoute->next;
  }

  return maxRoute;
}

/*---------------------------------------------------------------------
 * Method: decrementTTL(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function decrements the TTL by 1 for forwarding. Also acts appropriately if TTL is 0. 
 *---------------------------------------------------------------------*/
bool decrementTTL(struct sr_instance* sr, uint8_t* packet) {
  sr_ip_hdr_t *iphdr = getIPHeader(packet);
  iphdr->ip_ttl--;
  if(iphdr->ip_ttl == 0) {
    return false;
  }
  return true;
}

/*---------------------------------------------------------------------
 * Method: recomputeCheckSum(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function sets the checksum bytes to 0 and recomputes the checksum.
 *---------------------------------------------------------------------*/
void recomputeCheckSum(struct sr_instance* sr, uint8_t* packet) {
  sr_ip_hdr_t *iphdr = getIPHeader(packet);
  iphdr->ip_sum = 0;
  iphdr->ip_sum = cksum(iphdr, sizeof(sr_ip_hdr_t));
}


/*---------------------------------------------------------------------
 * Method: getICMPHeader(uint8_t* packet)
 * Scope:  Global
 *
 * Returns a pointer to the ICMP header in the IP Packet.
 *---------------------------------------------------------------------*/
sr_icmp_hdr_t* getICMPHeader(uint8_t* packet) {
  return (sr_icmp_hdr_t*)((char*)packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
}

sr_tcp_hdr_t* getTCPHeader(uint8_t* packet) {
  return (sr_tcp_hdr_t*)((char*)packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
}

/*---------------------------------------------------------------------
 * Method: getICMPT3Header(uint8_t* packet)
 * Scope:  Global
 *
 * Returns a pointer to the ICMPT3 header in the IP Packet.
 *---------------------------------------------------------------------*/
sr_icmp_t3_hdr_t* getICMPT3Header(uint8_t* packet) {
  return (sr_icmp_t3_hdr_t*)((char*)packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
}
/*---------------------------------------------------------------------
 * Method: readICMPHeader(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function reads the ICMP Header and returns the type of ICMP Request made.
 *---------------------------------------------------------------------*/
uint8_t readICMPHeader(struct sr_instance* sr, uint8_t* packet) {
  sr_icmp_hdr_t* ICMPHeader = getICMPHeader(packet);
  return ICMPHeader->icmp_type;
}


/*---------------------------------------------------------------------
 * Method: typeOfProtocol(uint8_t* packet)
 * Scope:  Global
 *
 * This function returns the protocol used, as specified in the IP Header. 
 *---------------------------------------------------------------------*/
uint8_t typeOfProtocol(uint8_t* packet) {
  sr_ip_hdr_t *iphdr = getIPHeader(packet);
  return iphdr->ip_p;
}

/*---------------------------------------------------------------------
 * Method: readBadHeader(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function is no-op. Just drops the packet if the type of protocol is not TCP/UDP/ICMP.
 *---------------------------------------------------------------------*/
void readBadHeader(struct sr_instance* sr, uint8_t* packet) {

}


/*---------------------------------------------------------------------
 * Method: originalICMPCheckSumValid
 * Scope:  Global
 *
 * This function verifies the checksum for a supplied ICMP header and returns true if verified. Else, returns false.
 *---------------------------------------------------------------------*/
bool originalICMPCheckSumValid(uint8_t * packet, unsigned int len) {
  sr_icmp_hdr_t* ICMPHeader = getICMPHeader(packet);
  uint16_t verifyCheckSumValue = cksum(ICMPHeader,  (len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t)));
  verifyCheckSumValue = ~verifyCheckSumValue;
  if(verifyCheckSumValue == 0) {
    return true;
  }
  return false;
}

/*---------------------------------------------------------------------
 * Method: sendFrameToNextHopForSelf
 * Scope:  Global
 * This function is called to send ICMP T3 replies. It first checks if there is a IP->MAC mapping
 * in the cache. If there is, it assigns the correct ethernet type, destination MAC, source MAC, and sends it over.
 * Otherwise, it adds the request to the cache, and immediately sends an ARP request to find the MAC Address.
 *
 *---------------------------------------------------------------------*/

void sendFrameToNextHopForSelf(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface, struct sr_rt* routeEntry) {
    struct sr_arpentry* currentEntry = sr_arpcache_lookup(&(sr->cache), routeEntry->gw.s_addr);
    if(currentEntry != NULL) { /* mapping found */
      sr_ethernet_hdr_t* etherNetHeader = (sr_ethernet_hdr_t*)(packet); 
      etherNetHeader->ether_type = htons(ethertype_ip); /*  IP Packet */
      memcpy((char*)packet, currentEntry->mac, ETHER_ADDR_LEN); /* Set destination MAC */
      struct sr_if* sourceInterface = sr_get_interface(sr, routeEntry->interface); /* set source MAC*/
      sr_send_packet(sr, packet, len, sourceInterface->name);
      free(packet);
      free(currentEntry);
    }
    else {
      printf("Sending ARP request...\n");
      struct sr_arpreq* currRequest = sr_arpcache_queuereq(&(sr->cache), routeEntry->gw.s_addr, 
        packet, len, routeEntry->interface); /* queue it */
      if(currRequest && currRequest->times_sent == 0) { /* if new request */
        sendARPRequest(sr, currRequest->ip); /* send ARP! */
        currRequest->sent = time(NULL);
        currRequest->times_sent++;
      }
    }
}

/*---------------------------------------------------------------------
 * Method: longestPrefixMatchWithDirectIP
 * Scope:  Global
 * Similar to longestPrefixMatch in function, except it takes in a destination IP and returns the 
 * entry in the routing table that has the longest match with the given IP. the IP is in network byte order.
 *---------------------------------------------------------------------*/
struct sr_rt* longestPrefixMatchWithDirectIP(struct sr_instance* sr, uint32_t ip) {
  struct sr_rt* currentRoute = sr->routing_table;
  int maxPrefixLength = -1;
  struct sr_rt* maxRoute = NULL;
  while(currentRoute != NULL) {
    int currentPrefixLength = runAlgorithmLongestPrefixMatch(currentRoute, ip);
    if(currentPrefixLength != -1) {
      if(currentPrefixLength > maxPrefixLength) {
        maxPrefixLength = currentPrefixLength;
        maxRoute = currentRoute;
      }
    }
    currentRoute = currentRoute->next;
  }
  return maxRoute;
}


/*---------------------------------------------------------------------
 * Method: sendEchoReply
 * Scope:  Global
 * This generates a ICMP echo reply in response to an ICMP echo request and sends it over to the
 * right host. 
 *---------------------------------------------------------------------*/

void sendEchoReply(struct sr_instance* sr, uint8_t* packet, unsigned int len) {
  /*printf("Sending echo reply...\n");*/
  if(originalICMPCheckSumValid(packet, len)) { /* if ICMP is valid */
    void* newPacket = malloc(len);
    memcpy(newPacket, packet, len);
    memcpy((char*)newPacket, (char*)packet + ETHER_ADDR_LEN, ETHER_ADDR_LEN); 
    sr_ip_hdr_t* iphdr = getIPHeader((uint8_t*)newPacket);
    iphdr->ip_id = htons(identificationNumber); /* IP id */
    identificationNumber++;
    iphdr->ip_ttl = MAX_TTL;
    /* Swap destination and source IPs */
    uint32_t temp = ntohl(iphdr->ip_src);
    iphdr->ip_src = iphdr->ip_dst;
    iphdr->ip_dst = htonl(temp);
    iphdr->ip_sum = 0;
    iphdr->ip_sum = cksum(iphdr, sizeof(sr_ip_hdr_t));

    /* build ICMP header */
    sr_icmp_hdr_t* ICMPHeader = getICMPHeader(newPacket);
    ICMPHeader->icmp_type = 0;
    ICMPHeader->icmp_sum = 0;
    ICMPHeader->icmp_sum = cksum(ICMPHeader, (len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t)));
    struct sr_rt* routeEntry = longestPrefixMatchWithDirectIP(sr, iphdr->ip_dst); /* Find entry for next hop */
    if(routeEntry != NULL) {
      struct sr_if* sourceInterface = sr_get_interface(sr, routeEntry->interface);
      if(sourceInterface != NULL) {
        memcpy((char*)newPacket + ETHER_ADDR_LEN, sourceInterface->addr, ETHER_ADDR_LEN); /* Set source MAC */
        sendFrameToNextHopForSelf(sr, (uint8_t*)newPacket, len, sourceInterface->name, routeEntry);
       /* print_hdrs((uint8_t*)newPacket, len);*/
      }
    }
  }
}


/*---------------------------------------------------------------------
 * Method: sendICMPReply(struct sr_instance* sr, uint8_t* packet, uint8_t codeReceived)
 * Scope:  Global
 *
 * This function creates a new ICMP reply with the specified type and code and sends it to the 
 * next hop.
 *---------------------------------------------------------------------*/
void sendICMPReply(struct sr_instance* sr, uint8_t* packet, uint8_t type, uint8_t code) {
 void* newPacket = malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
 sr_ethernet_hdr_t* etherNetHeader = (sr_ethernet_hdr_t*)packet;
 etherNetHeader->ether_type = htons(ethertype_ip); /* IP type */

 sr_ip_hdr_t* iphdr = getIPHeader((uint8_t*)newPacket);
 sr_ip_hdr_t* oldIphdr = getIPHeader((uint8_t*)packet);
 iphdr->ip_hl = oldIphdr->ip_hl;
 iphdr->ip_v = oldIphdr->ip_v;
 iphdr->ip_tos = 0;
 iphdr->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t)); /* IP length */
 iphdr->ip_id = htons(identificationNumber); /* Unique ID */
 identificationNumber++;
 iphdr->ip_off = htons(0);
 iphdr->ip_ttl = MAX_TTL; /* Time to live */
 iphdr->ip_p = ip_protocol_icmp; /* ICMP protocol */
 iphdr->ip_dst = oldIphdr->ip_src; 

 struct sr_rt* routeEntry = longestPrefixMatch(sr, newPacket); /* Find entry for next hop */
 if(routeEntry != NULL) {
   struct sr_if* sourceInterface = sr_get_interface(sr, routeEntry->interface);
   if(sourceInterface != NULL) {
      iphdr->ip_src = sourceInterface->ip; /* Source IP is the one at the routing interface */
      iphdr->ip_sum = 0;
      iphdr->ip_sum = cksum(iphdr, sizeof(sr_ip_hdr_t)); /* Calculate checksum */
      sr_icmp_t3_hdr_t* ICMPHeader = getICMPT3Header(newPacket);
      /* Build ICMPT3 header */
      ICMPHeader->icmp_type = type;
      ICMPHeader->icmp_code = code;
      memcpy(ICMPHeader->data, (char*)packet + sizeof(sr_ethernet_hdr_t), ICMP_DATA_SIZE);
      ICMPHeader->icmp_sum = 0;
      ICMPHeader->icmp_sum = cksum(ICMPHeader, sizeof(sr_icmp_t3_hdr_t));
      memcpy((char*)newPacket + ETHER_ADDR_LEN, sourceInterface->addr, ETHER_ADDR_LEN);
      sendFrameToNextHopForSelf(sr, newPacket, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t), sourceInterface->name, routeEntry);
    }
  }
}

/*---------------------------------------------------------------------
 * Method: sendPortUnreachable
 * Scope:  Global
 *
 * This function is a wrapper to send a Port Unreachable ICMP reply. 
 *---------------------------------------------------------------------*/
void sendPortUnreachable(struct sr_instance* sr, uint8_t* packet) {
  sendICMPReply(sr, packet, 3, 3);
} 

/*---------------------------------------------------------------------
 * Method: sendDestinationNetUnreachable
 * Scope:  Global
 *
 * This function is a wrapper to send a Destination Net ICMP reply. 
 *---------------------------------------------------------------------*/
void sendDestinationNetUnreachable(struct sr_instance* sr, uint8_t* packet) {
  sendICMPReply(sr, packet, 3, 0);
}

/*---------------------------------------------------------------------
 * Method: sendDestinationHostUnreachable
 * Scope:  Global
 *
 * This function is a wrapper to send a Destination Host ICMP reply. 
 *---------------------------------------------------------------------*/
void sendDestinationHostUnreachable(struct sr_instance* sr, uint8_t* packet) {
  sendICMPReply(sr, packet, 3, 1);
}

/*---------------------------------------------------------------------
 * Method: sendTimeExceeded
 * Scope:  Global
 *
 * Increases the TTL by 1 in the original packet and recomputes checksum (since it needs to be sent 
 * with the ICMP reply), and calls the function that generates the ICMP reply 
 *---------------------------------------------------------------------*/
void sendTimeExceeded(struct sr_instance* sr, uint8_t* packet) {
  sr_ip_hdr_t* iphdr = getIPHeader(packet);
  iphdr->ip_ttl++;
  recomputeCheckSum(sr, packet);
  sendICMPReply(sr, packet, 11, 0);
}

/*---------------------------------------------------------------------
 * Method: interpretPacketForSelf(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This function is designed to interpret the IP Packet if it was meant for one of the router's IPs. It
 * looks at the type of protocol, in case it's ICMP and is an echo request, it sends an ICMP echo reply.
 * Else if it receives a TCP/UDP header, it sends back an ICMP Port Unreachable reply.
 *---------------------------------------------------------------------*/
void interpretPacketForSelf(struct sr_instance* sr, uint8_t* packet, unsigned int len) {
  switch(typeOfProtocol(packet)) {
    case 1 : 
      /*sendICMPReply(sr, packet, 0, 0); break;*/
      sendEchoReply(sr, packet, len); break;
    case 6 : sendPortUnreachable(sr, packet); break;
    case 17 : sendPortUnreachable(sr, packet); break;
    default : readBadHeader(sr, packet);
  }
}

/*---------------------------------------------------------------------
 * Method: sendFrameToNextHop(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface)
 * Scope:  Global
 *
 * This function forwards the ethernet frame to the next hop. 
 * It first finds the route entry that matches the prefix of the IP, and then looks up
 * the next hop IPs MAC address in the cache. If it exists, it sets the destination MAC address
 * and source MAC adress and sends the packet. Otherwise, it puts it into the queue and sends an ARP 
 * request to fetch the MAC Address. In case it cannot find a route, it sends a Destination Net Unreachable 
 * ICMP reply.
 *---------------------------------------------------------------------*/
void sendFrameToNextHop(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  void* newPacket = malloc(len);
  memcpy(newPacket, packet, len);
  struct sr_rt* routeEntry = longestPrefixMatch(sr, (uint8_t*)newPacket);
  if(routeEntry != NULL) {
    struct sr_arpentry* currentEntry = sr_arpcache_lookup(&(sr->cache), routeEntry->gw.s_addr);
    if(currentEntry != NULL) {
      memcpy((char*)newPacket, currentEntry->mac, ETHER_ADDR_LEN);
      struct sr_if* sourceInterface = sr_get_interface(sr, routeEntry->interface);
      memcpy((char*)newPacket + ETHER_ADDR_LEN, sourceInterface->addr, ETHER_ADDR_LEN);
      sr_send_packet(sr, (uint8_t *)newPacket, len, sourceInterface->name);
      free(newPacket);
      free(currentEntry);
    }
    else {
      struct sr_arpreq* currRequest = sr_arpcache_queuereq(&(sr->cache), routeEntry->gw.s_addr, 
        (uint8_t*)newPacket, len, routeEntry->interface);
      if(currRequest && currRequest->times_sent == 0) {
        sendARPRequest(sr, currRequest->ip);
        currRequest->sent = time(NULL);
        currRequest->times_sent++;
      }
    }
  }
  else {
      sendDestinationNetUnreachable(sr, packet); /*10 is a constant denoting destination not reachable*/
  }
}


/*---------------------------------------------------------------------
 * Method: sendARPRequest
 * Scope:  Global
 *
 * This method, sends an ARP request to the next hop IP supplied.
 * It does so by generating the ARP request : setting the destintion MAC Address to broadcast, type to ARP,
 * and finally setting the variables in the ARP header.
 *---------------------------------------------------------------------*/
void sendARPRequest(struct sr_instance* sr, uint32_t ip) {
 bool alreadyFree = false; 
 void* newPacket = malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t));
 sr_arp_hdr_t* ARPHeader = (sr_arp_hdr_t*)((char*)newPacket + sizeof(sr_ethernet_hdr_t));
 sr_ethernet_hdr_t* etherNetHeader = (sr_ethernet_hdr_t*)newPacket;
 int i = 0;
 for(i = 0; i < ETHER_ADDR_LEN; i++) { /* set to broadcast */
    etherNetHeader->ether_dhost[i] = 0xff;
 }
 etherNetHeader->ether_type = htons(ethertype_arp);
 ARPHeader->ar_hrd = htons(arp_hrd_ethernet); /* Ethernet type */
 ARPHeader->ar_pro = htons(ethertype_ip); /* IP */
 ARPHeader->ar_hln = 6;
 ARPHeader->ar_pln = 4;
 ARPHeader->ar_op = htons(1); /* Request */

 for(i = 0; i < ETHER_ADDR_LEN; i++) { /* Target address is broadcast */
  ARPHeader->ar_tha[i] = 0xff;
 }
 ARPHeader->ar_tip = ip;
 struct sr_rt* routeEntry = longestPrefixMatchWithDirectIP(sr, ip); 
 if(routeEntry != NULL) {
  struct sr_if* sourceInterface = sr_get_interface(sr, routeEntry->interface);
  if(sourceInterface != NULL) {
    memcpy((char*)newPacket + ETHER_ADDR_LEN, sourceInterface->addr, ETHER_ADDR_LEN);
    memcpy(ARPHeader->ar_sha, sourceInterface->addr, ETHER_ADDR_LEN);
    ARPHeader->ar_sip = sourceInterface->ip;
    sr_send_packet(sr, (uint8_t*)newPacket, sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t), sourceInterface->name);
    free(newPacket);
    alreadyFree = true; 
  }
 }
 if(alreadyFree == false) { /* If no route found */
  free(newPacket);
 }

}


/*---------------------------------------------------------------------
 * Method: interpretPacketForForwarding(struct sr_instance* sr, uint8_t* packet)
 * Scope:  Global
 *
 * This method processes the IP Packet by first decrementing TTL, and if it is 0, sending a Time exceeded
 * ICMP reply. Otherwise, it recomputes the checksum and forwards it to the next hop-ip.
 *
 *---------------------------------------------------------------------*/
void interpretPacketForForwarding(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  if(decrementTTL(sr, packet)) {
    recomputeCheckSum(sr, packet);
    sendFrameToNextHop(sr, packet, len, interface);
  }
  else {
    sendTimeExceeded(sr, packet);
  }
}

/*---------------------------------------------------------------------
 * Method: processIPPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface)
 * Scope:  Global
 *
 * This method processes the IP Packet in the Ethernet Frame. It first checks the 
 * validity of the IP Packet, and if it is valid, then checks if the IP Packet is for
 * the router or not. If it is, it interprets the packet for itself, and otherwise interprets
 * it for forwarding.
 *---------------------------------------------------------------------*/
void processIPPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  if(checkifValid(packet, len)) {
    if(!isNatEnabled(sr)) {
      int interfaceMatchFound = checkListOfInterfaces(sr, packet);
      int ipMatchFound = checkListOfIPs(sr, packet);
      if(interfaceMatchFound == 1 && ipMatchFound == 1) {
        interpretPacketForSelf(sr, packet, len);
      }
      else if(interfaceMatchFound == 1 && ipMatchFound == 0) {
        interpretPacketForForwarding(sr, packet, len, interface);
      }
    }
    else {
      printf("Will process with NAT...\n");
      natProcessIPPacket(sr, packet, len, interface);
    }
  }
}
/*---------------------------------------------------------------------
 * Method: sendARPReply
 * Scope:  Global
 *
 * This method, sends an ARP reply in response to an incoming ARP request.
 * It does so by generating the ARP reply and sending the packet.
*---------------------------------------------------------------------*/
void sendARPReply(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
 bool alreadyFree = false;
 void* newPacket = malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t));
 sr_ethernet_hdr_t* ethernetHeader = (sr_ethernet_hdr_t*)newPacket;
 ethernetHeader->ether_type = htons(ethertype_arp);
 sr_arp_hdr_t* ARPHeader = (sr_arp_hdr_t*)((char*)newPacket + sizeof(sr_ethernet_hdr_t));
 sr_arp_hdr_t* oldARPHeader = (sr_arp_hdr_t*)((char*)packet + sizeof(sr_ethernet_hdr_t));
 ARPHeader->ar_hrd = htons(arp_hrd_ethernet);
 ARPHeader->ar_pro = htons(ethertype_ip);
 ARPHeader->ar_hln = 6;
 ARPHeader->ar_pln = 4;
 ARPHeader->ar_op = htons(2);
 struct sr_if* currInterface = sr_get_interface(sr, interface);
 ARPHeader->ar_sip = currInterface->ip; /* Source IP is the interface IP that the original packet came into */
 ARPHeader->ar_tip = oldARPHeader->ar_sip; /* Target IP is the old packet's source IP */
 memcpy(ARPHeader->ar_tha, oldARPHeader->ar_sha, ETHER_ADDR_LEN); /* Target Hardware Add is the old packet's Source Hardware Add */

 struct sr_rt* routeEntry = longestPrefixMatchWithDirectIP(sr, ARPHeader->ar_tip); /* Find route */
 if(routeEntry != NULL) {
  struct sr_if* sourceInterface = sr_get_interface(sr, routeEntry->interface);
  if(sourceInterface != NULL) {
    sr_arpcache_insert(&(sr->cache), oldARPHeader->ar_sha, oldARPHeader->ar_sip); /* Put the IP->MAC mapping received into the cache */
    memcpy((char*)newPacket + ETHER_ADDR_LEN, sourceInterface->addr, ETHER_ADDR_LEN);
    memcpy((char*)newPacket, oldARPHeader->ar_sha, ETHER_ADDR_LEN);
    memcpy(ARPHeader->ar_sha, currInterface->addr, ETHER_ADDR_LEN);
    sr_send_packet(sr, (uint8_t*)newPacket, sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t), sourceInterface->name);
    free(newPacket);
    alreadyFree = true;
  }
 }
 if(!alreadyFree) {
  free(newPacket);
 }
}

/*---------------------------------------------------------------------
 * Method: interpretARPPacket
 * Scope:  Global
 *
 * Wrapper for processing the ARP Packet. If it's an ARP request, an appropriate reply is sent.
 * Otherwise, if it's a reply, the reply is inserted into the cache. If there are any pending packets
 * on that reply, their ethernet headers are filled in and they are sent
*---------------------------------------------------------------------*/
void interpretARPPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  sr_arp_hdr_t* ARPHeader = (sr_arp_hdr_t *)((char*)packet + sizeof(sr_ethernet_hdr_t));
  if(ntohs(ARPHeader->ar_op) == arp_op_request) { /* request */
    sendARPReply(sr, packet, len, interface);
  }
  else if (ntohs(ARPHeader->ar_op) == arp_op_reply) { /* reply */
    struct sr_arpreq* currRequest = sr_arpcache_insert(&(sr->cache), ARPHeader->ar_sha, ARPHeader->ar_sip);
    if(currRequest) {
      struct sr_packet* currPacket = currRequest->packets;
      struct sr_rt* routeEntry; 
      sr_ip_hdr_t* iphdr;
      struct sr_if* sourceInterface;
      while(currPacket != NULL) {
        iphdr = getIPHeader(currPacket->buf);
        routeEntry = longestPrefixMatchWithDirectIP(sr, iphdr->ip_dst);
        if(routeEntry != NULL) {
          sourceInterface = sr_get_interface(sr, routeEntry->interface);
          memcpy((char*)(currPacket->buf) + ETHER_ADDR_LEN, sourceInterface->addr, ETHER_ADDR_LEN);
          memcpy((char*)currPacket->buf, (char*)ARPHeader->ar_sha, ETHER_ADDR_LEN);
          sr_ethernet_hdr_t* ethernetHeader = (sr_ethernet_hdr_t*)(currPacket->buf);
          ethernetHeader->ether_type = htons(ethertype_ip);
          sr_send_packet(sr, currPacket->buf, currPacket->len, currPacket->iface);
        }
        currPacket = currPacket->next;
      }
      sr_arpreq_destroy(&(sr->cache), currRequest);
    }
  }
}

/*---------------------------------------------------------------------
 * Method: checkListOfARPIPs
 * Scope:  Global
 *
 * This function goes over all the interfaces and returns true if the 
 * ip associated to the interface where the ARP packet is received is same as
 * the target IP in the ARP packet. 
*---------------------------------------------------------------------*/
int checkListOfARPIPs(struct sr_instance* sr, uint8_t* packet, char* interface) {
 struct sr_if* currentInterface = sr->if_list;
  sr_arp_hdr_t* ARPHeader = (sr_arp_hdr_t *)((char*)packet + sizeof(sr_ethernet_hdr_t));
  while(currentInterface != NULL) {
    if((strcmp(interface,currentInterface->name) == 0) && (currentInterface->ip == ARPHeader->ar_tip)) {
      return 1;
    }
    currentInterface = currentInterface->next;
  }
  return 0;
}

/*---------------------------------------------------------------------
 * Method: processARPPacket
 * Scope:  Global
 *
 * This is a wrapper for the packet to be interpreted. It is only interpreted if 
 * the match is found. Else, it's dropped.
*---------------------------------------------------------------------*/
void processARPPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  int ipMatchFound = checkListOfARPIPs(sr, packet, interface);
  if(ipMatchFound) {
    interpretARPPacket(sr, packet, len, interface);
  }

}

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/
void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */)
{
  printf("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx NEW REQUEST xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);
  uint16_t etherType = ethertype(packet);
  if(etherType == ethertype_ip) {
    printf("Received IP Packet, length(%d)\n", len);
    processIPPacket(sr, packet, len, interface);
  }
  else if (etherType == ethertype_arp) {
    printf("Received ARP Packet, length(%d)\n", len);
    processARPPacket(sr, packet, len, interface);
  }
}
/* end sr_ForwardPacket */

