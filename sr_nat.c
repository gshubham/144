
#include <signal.h>
#include <assert.h>
#include <unistd.h>

#include "sr_nat.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sr_router.h"
#include "sr_utils.h"
#include <stdbool.h>

static char internalInterface[] = "eth1";

static struct sr_if* getInternalInterface(struct sr_instance* sr) {
  return sr_get_interface(sr, internalInterface);
}

/* Adopted from sr_arpreq_destroy from sr_arpcache.c */

void removeMapping(struct sr_nat* nat, sr_nat_mapping_t* mapping) {
  if(mapping) {
    sr_nat_mapping_t *currMapping, *prevMapping = NULL, *nextMapping = NULL;
    for (currMapping = nat->mappings; currMapping != NULL; currMapping = currMapping->next) {
      if(currMapping == mapping) {
        if(prevMapping) {
          nextMapping = currMapping->next;
          prevMapping->next = nextMapping;
        }
        else {
          nextMapping = currMapping->next;
          nat->mappings = nextMapping;
        }
        break;
      }
      prevMapping = currMapping;
    }
    while(mapping->conns != NULL) {
      sr_nat_connection_t* currConnection = mapping->conns;
      mapping->conns = currConnection->next;
      free(currConnection);
    }
    free(mapping);
  }
}

void removeConnection(sr_nat_mapping_t* mapping, sr_nat_connection_t* connection) {
  if(mapping && connection) {
    sr_nat_connection_t *currConnection, *prevConnection = NULL, *nextConnection = NULL;
    for (currConnection = mapping->conns; currConnection != NULL; currConnection = currConnection->next) {
      if(currConnection == connection) {
        if(prevConnection) {
          nextConnection = currConnection->next;
          prevConnection->next = nextConnection;
        }
        else {
          nextConnection = currConnection->next;
          mapping->conns = nextConnection;
        }
        break;
      }
      prevConnection = currConnection;
    }
    if(connection->inBoundSyn != NULL) {
      free(connection->inBoundSyn);
    }
    free(connection);
  }
}
sr_nat_mapping_t * natLookupExternal(struct sr_nat *nat,
    uint16_t aux_ext, sr_nat_mapping_type type ) {
  sr_nat_mapping_t * currentMapping = nat->mappings;
  while(currentMapping != NULL) {
    if(currentMapping->type == type && currentMapping->aux_ext == aux_ext) {
      return currentMapping;
    }
    currentMapping = currentMapping->next;
  } 
  return NULL;
}

sr_nat_mapping_t* natLookupInternal(struct sr_nat *nat, uint32_t ip_int, uint16_t aux_int,
  sr_nat_mapping_type type) {
  sr_nat_mapping_t * currentMapping = nat->mappings;
  while(currentMapping != NULL) {
    if((currentMapping->type == type) && (currentMapping->aux_int == aux_int) && 
      (currentMapping->ip_int == ip_int)) {
      return currentMapping;
    }
    currentMapping = currentMapping->next;
  }
  return NULL;
}
uint16_t getNextMapping(struct sr_nat* nat, sr_nat_mapping_type type) {
  uint16_t currentNumber;
  if(type == nat_mapping_icmp) {
    currentNumber = nat->nextICMPIdentifier;
  }
  else {
    currentNumber = nat->nextTCPPort;
  }
  sr_nat_mapping_t* currentMapping = nat->mappings;

  while(currentMapping != NULL) {
    if(currentMapping->type == type && htons(currentNumber) == currentMapping->aux_ext) {
      if(currentNumber == END_NUM) {
        currentNumber = BEGIN_NUM;
      }
      else {
        currentNumber++;
      }
      currentMapping = nat->mappings;
    }
    else {
      currentMapping = currentMapping->next;
    }
  }
  if(type == nat_mapping_icmp) {
    if(currentNumber == END_NUM) {
      nat->nextICMPIdentifier = BEGIN_NUM;
    }
    else {
      nat->nextICMPIdentifier = currentNumber++;
    }
  }
  else {
    if(currentNumber == END_NUM) {
      nat->nextTCPPort = BEGIN_NUM;
    }
    else {
      nat->nextTCPPort = currentNumber++;
    }
  }
  return currentNumber;
}
sr_nat_mapping_t* natCreateMapping(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {
  sr_nat_mapping_t* newMapping = malloc(sizeof(sr_nat_mapping_t));
  newMapping->conns = NULL;
  newMapping->aux_int = aux_int;
  newMapping->ip_int = ip_int;
  newMapping->last_updated = time(NULL);
  newMapping->type = type;
  newMapping->next = nat->mappings;
  uint16_t nextMapping = getNextMapping(nat, type);
  /*printf("The next mapping is %d\n", nextMapping);*/
  newMapping->aux_ext = htons(nextMapping);
  nat->mappings = newMapping;
  return newMapping;
}

int sr_nat_init(struct sr_nat *nat) { /* Initializes the nat */

  assert(nat);

  /* Acquire mutex lock */
  pthread_mutexattr_init(&(nat->attr));
  pthread_mutexattr_settype(&(nat->attr), PTHREAD_MUTEX_RECURSIVE);
  int success = pthread_mutex_init(&(nat->lock), &(nat->attr));

  /* Initialize timeout thread */

  pthread_attr_init(&(nat->thread_attr));
  pthread_attr_setdetachstate(&(nat->thread_attr), PTHREAD_CREATE_JOINABLE);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_create(&(nat->thread), &(nat->thread_attr), sr_nat_timeout, nat);

  /* CAREFUL MODIFYING CODE ABOVE THIS LINE! */

  nat->mappings = NULL;
  /* Initialize any variables here */
  nat->nextICMPIdentifier = BEGIN_NUM;
  nat->nextTCPPort = BEGIN_NUM;
  return success;
}


int sr_nat_destroy(struct sr_nat *nat) {  /* Destroys the nat (free memory) */

  pthread_mutex_lock(&(nat->lock));

  /* free nat memory here */
  while(nat->mappings != NULL) {
    removeMapping(nat, nat->mappings);
  }

  pthread_kill(nat->thread, SIGKILL);
  return pthread_mutex_destroy(&(nat->lock)) &&
    pthread_mutexattr_destroy(&(nat->attr));

}

void *sr_nat_timeout(void *nat_ptr) {  /* Periodic Timout handling */
  struct sr_nat *nat = (struct sr_nat *)nat_ptr;
  while (1) {
    sleep(1.0);
    pthread_mutex_lock(&(nat->lock));

    time_t curtime = time(NULL);
    /* handle periodic tasks here */
    sr_nat_mapping_t* currentMapping = nat->mappings;
    while(currentMapping != NULL) {
      if(currentMapping->type == nat_mapping_icmp) {
        if(difftime(curtime, currentMapping->last_updated) > nat->icmpQueryTimeout) {
          sr_nat_mapping_t* nextMapping = currentMapping->next;
          removeMapping(nat, currentMapping);
          currentMapping = nextMapping;
        }
        else {
          currentMapping = currentMapping->next;
        }
      }
      else if(currentMapping->type == nat_mapping_tcp) {
        sr_nat_connection_t* currentConnection = currentMapping->conns;
        while(currentConnection != NULL) {
          if(currentConnection->connectionState == connected && difftime(curtime, 
            currentConnection->lastAccessed) > nat->tcpEstablishedIdleTimeout) {
              sr_nat_connection_t* nextConnection = currentConnection->next;
              removeConnection(currentMapping, currentConnection);
              currentConnection = nextConnection;
          }
          else if (((currentConnection->connectionState == outBoundSyn) || 
            (currentConnection->connectionState == timeWait)) && difftime(curtime,
            currentConnection->lastAccessed) > nat->tcpTransitoryIdleTimeout) {
              sr_nat_connection_t* nextConnection = currentConnection->next;
              removeConnection(currentMapping, currentConnection);
              currentConnection = nextConnection;       
          }
          else if(currentConnection->connectionState == inBoundSynPending && difftime(curtime,
            currentConnection->lastAccessed) > nat->tcpTransitoryIdleTimeout) {
              sr_nat_connection_t* nextConnection = currentConnection->next;
              if(currentConnection->inBoundSyn != NULL) {
                /*TODO : Send a type three response */
              }          
              removeConnection(currentMapping, currentConnection);
              currentConnection = nextConnection;  
          }
          else {
            currentConnection = currentConnection->next;
          }
        }
        if(currentMapping->conns == NULL) {
          sr_nat_mapping_t* nextMapping = currentMapping->next;
          removeMapping(nat, currentMapping);
          currentMapping = nextMapping; 
        }
        else {
          currentMapping = currentMapping->next;
        }
      }
      else {
        currentMapping = currentMapping->next;
      }
    }

    pthread_mutex_unlock(&(nat->lock));
  }
  return NULL;
}

/* Get the mapping associated with given external port.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_external(struct sr_nat *nat,
    uint16_t aux_ext, sr_nat_mapping_type type ) {

  /*printf("Entering lookup... \n");*/
  pthread_mutex_lock(&(nat->lock));
  /*printf("In the external lock... \n");*/
  /* handle lookup here, malloc and assign to copy */
  struct sr_nat_mapping *copy = NULL;
  sr_nat_mapping_t* lookup = natLookupExternal(nat, aux_ext, type);
  if(lookup != NULL) {
    lookup->last_updated = time(NULL);
    copy = malloc(sizeof(sr_nat_mapping_t));
    memcpy(copy, lookup, sizeof(sr_nat_mapping_t));
  }
 /* printf("Leaving the lock..\n");*/
  pthread_mutex_unlock(&(nat->lock));
 /* printf("Out of lock...\n");*/
  return copy;
}

/* Get the mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_internal(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  /*printf("Entering lookup... \n");*/
  pthread_mutex_lock(&(nat->lock));
 /* printf("In the internal lock... \n");*/
  /* handle lookup here, malloc and assign to copy. */
  struct sr_nat_mapping *copy = NULL;
  sr_nat_mapping_t* lookup = natLookupInternal(nat, ip_int, aux_int, type);
  if(lookup != NULL) {
    lookup->last_updated = time(NULL);
    copy = malloc(sizeof(sr_nat_mapping_t));
    memcpy(copy, lookup, sizeof(sr_nat_mapping_t));
  }  
 /* printf("Leaving the lock..\n");*/
  pthread_mutex_unlock(&(nat->lock));
 /* printf("Out of lock...\n");*/
  return copy;
}

/* Insert a new mapping into the nat's mapping table.
   Actually returns a copy to the new mapping, for thread safety.
 */
struct sr_nat_mapping *sr_nat_insert_mapping(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

 /* printf("Entering Insert... \n");*/
  pthread_mutex_lock(&(nat->lock));
  /*printf("Inside Insert ... \n");*/
  /* handle insert here, create a mapping, and then return a copy of it */
  struct sr_nat_mapping *mapping = natCreateMapping(nat, ip_int, aux_int, type);
  struct sr_nat_mapping *copy = malloc(sizeof(sr_nat_mapping_t));
  memcpy(copy, mapping, sizeof(sr_nat_mapping_t));
 /* printf("Leaving insert... \n");*/
  pthread_mutex_unlock(&(nat->lock));
/*  printf("Left insert ...\n");*/
  return copy;
}

void recomputeTCPCheckSum(uint8_t* packet, unsigned int len) {
  sr_ip_hdr_t* iphdr = getIPHeader(packet);
  unsigned int tcpLen = len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t);
  uint8_t* tempCopy = malloc(sizeof(sr_tcp_pseudo_hdr_t) + tcpLen);
  sr_tcp_pseudo_hdr_t * pseudoChecksum = (sr_tcp_pseudo_hdr_t *) tempCopy;
  sr_tcp_hdr_t* tcpHeader = getTCPHeader(packet);
  memcpy(tempCopy + sizeof(sr_tcp_pseudo_hdr_t), tcpHeader, tcpLen);
  pseudoChecksum->src = iphdr->ip_src;
  pseudoChecksum->dst = iphdr->ip_dst;
  pseudoChecksum->zeroes = 0;
  pseudoChecksum->p = ip_protocol_tcp;
  pseudoChecksum->tcpLen = htons(tcpLen);

  tcpHeader->checkSum = 0;
  tcpHeader->checkSum = cksum(tempCopy, sizeof(sr_tcp_pseudo_hdr_t) + tcpLen);
  free(tempCopy);
}

void natProcessOutboundPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface, sr_nat_mapping_t* natLookup) {
  sr_ip_hdr_t* iphdr = getIPHeader(packet);
  if(iphdr->ip_p == ip_protocol_icmp) {
    sr_icmp_hdr_t* icmpHeader = getICMPHeader(packet);
    if(icmpHeader->icmp_type == 8 || icmpHeader->icmp_type == 0) {
      sr_icmp_echo_hdr_t* icmpEchoHeader = (sr_icmp_echo_hdr_t * ) icmpHeader;
      assert(natLookup);
      icmpEchoHeader->identifier = natLookup->aux_ext;
      icmpEchoHeader->icmp_sum = 0;
      icmpEchoHeader->icmp_sum = cksum(icmpEchoHeader, (len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t)));
      struct sr_rt* routeEntry = longestPrefixMatchWithDirectIP(sr, iphdr->ip_dst);
      iphdr->ip_src = sr_get_interface(sr, routeEntry->interface)->ip;
      interpretPacketForForwarding(sr, packet, len, interface);
    }
  }
  else if(iphdr->ip_p == ip_protocol_tcp) {
    sr_tcp_hdr_t * tcpHeader = getTCPHeader(packet);
    tcpHeader->s_port = natLookup->aux_ext;
    struct sr_rt* routeEntry = longestPrefixMatchWithDirectIP(sr, iphdr->ip_dst);
    iphdr->ip_src = sr_get_interface(sr, routeEntry->interface)->ip;
    recomputeTCPCheckSum(packet, len);
    interpretPacketForForwarding(sr, packet, len, interface);
  }
}

void natProcessInboundPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface, sr_nat_mapping_t* natLookup) {
  sr_ip_hdr_t* iphdr = getIPHeader(packet);
  if(iphdr->ip_p == ip_protocol_icmp) {
    sr_icmp_hdr_t* icmpHeader = getICMPHeader(packet);
    if(icmpHeader->icmp_type == 8 || icmpHeader->icmp_type == 0) {
      sr_icmp_echo_hdr_t* icmpEchoHeader = (sr_icmp_echo_hdr_t *) icmpHeader;
      icmpEchoHeader->identifier = natLookup->aux_int;
      icmpEchoHeader->icmp_sum = 0;
      icmpEchoHeader->icmp_sum = cksum(icmpEchoHeader, len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t));
      iphdr->ip_dst = natLookup->ip_int;
      interpretPacketForForwarding(sr, packet, len, interface);
    }
  }
  else if (iphdr->ip_p  == ip_protocol_tcp) {
    sr_tcp_hdr_t * tcpHeader = getTCPHeader(packet);
    tcpHeader->d_port = natLookup->aux_int;
    iphdr->ip_src = natLookup->ip_int;
    recomputeTCPCheckSum(packet, len);
    interpretPacketForForwarding(sr, packet, len, interface);
  }
}

void natInterpretPacketForForwarding(struct sr_instance*sr, uint8_t* packet, unsigned int len, char* interface) {
  printf("Interpreting for forwarding...\n");
  sr_icmp_hdr_t* icmpHeader = getICMPHeader(packet);
  if(icmpHeader->icmp_type == 8 || icmpHeader->icmp_type == 0) {
    sr_icmp_echo_hdr_t* icmpEchoHeader = (sr_icmp_echo_hdr_t*) icmpHeader;
    sr_nat_mapping_t * natLookup = sr_nat_lookup_internal(sr->nat, getIPHeader(packet)->ip_src, icmpEchoHeader->identifier, nat_mapping_icmp);
    if(natLookup == NULL) {
      printf("The identifier is .. %d\n", icmpEchoHeader->identifier);
      natLookup = sr_nat_insert_mapping(sr->nat, getIPHeader(packet)->ip_src, icmpEchoHeader->identifier, nat_mapping_icmp);
    }
    print_addr_ip_int(natLookup->ip_int);
    printf("The identifier used was ... %d\n", natLookup->aux_int);
    natProcessOutboundPacket(sr, packet, len, interface, natLookup);
    free(natLookup);
  }
}

void natInterpretInboundReceivedForSelf(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
 /* printf("Interpreting inbound for self...\n");*/
  sr_icmp_hdr_t* icmpHeader = getICMPHeader(packet);
  if(icmpHeader->icmp_type == 8 || icmpHeader->icmp_type == 0) {
    sr_icmp_echo_hdr_t* icmpEchoHeader = (sr_icmp_echo_hdr_t*) icmpHeader;
    sr_nat_mapping_t* natLookup = sr_nat_lookup_external(sr->nat, icmpEchoHeader->identifier, nat_mapping_icmp);
    if(natLookup == NULL) {
     /* printf("No mapping.., Just interpret it for yourself!\n");*/
      interpretPacketForSelf(sr, packet, len);
    }
    else {
      natProcessInboundPacket(sr, packet, len, interface, natLookup);
      free(natLookup);
    }
  }
}
void natInterpretInboundReceivedForForwarding(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  struct sr_rt* routeEntry = longestPrefixMatchWithDirectIP(sr, getIPHeader(packet)->ip_dst);
  if(routeEntry == NULL) {
    return;
  }
  if(getInternalInterface(sr)->ip != sr_get_interface(sr, routeEntry->interface)->ip) {
    interpretPacketForForwarding(sr, packet, len, interface);
  }
}

void natProcessICMPPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  if(originalICMPCheckSumValid(packet, len)) {
    printf("Checksum is valid...\n");
    struct sr_if* currentInterface = sr_get_interface(sr, interface);
    int interfaceMatchFound = checkListOfInterfaces(sr, packet);
    int ipMatchFound = checkListOfIPs(sr, packet);
    if(getInternalInterface(sr)->ip == currentInterface->ip) {
      if(interfaceMatchFound == 1 && ipMatchFound == 1) {
       /* printf("Just simply interpreting for self..\n");*/
        interpretPacketForSelf(sr, packet, len);
      }
      else if(interfaceMatchFound == 1 && ipMatchFound == 0) {
        natInterpretPacketForForwarding(sr, packet, len, interface);
      }
    }
    else {
      printf("Caught at external interface...\n");
      if(interfaceMatchFound == 1 && ipMatchFound == 0) {
        printf("Being stupid...\n");
        natInterpretInboundReceivedForForwarding(sr, packet, len, interface);
      }
      else if (interfaceMatchFound == 1 && ipMatchFound == 1) {
        printf("In here...\n");
        if(getIPHeader(packet)->ip_dst == getInternalInterface(sr)->ip) {
          printf("Returning... \n");
          return;
        }
        natInterpretInboundReceivedForSelf(sr, packet, len, interface);
      }
    }
  }
}

sr_nat_connection_t* findConnection(sr_nat_mapping_t* mapping, uint32_t ip, uint16_t port) {
  sr_nat_connection_t* currConnection = mapping->conns;
  while(currConnection != NULL) {
    if(currConnection->ip == ip && currConnection->port == port) {
      currConnection->lastAccessed = time(NULL);
      return currConnection;
    }
    currConnection = currConnection->next;
  }
  return NULL;
}

bool TCPPacketValid(uint8_t* packet, unsigned int len) {
  bool valid = false;
  sr_ip_hdr_t* iphdr = getIPHeader(packet);
  unsigned int tcpLen = len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t);
  uint8_t* tempCopy = malloc(sizeof(sr_tcp_pseudo_hdr_t) + tcpLen);
  sr_tcp_pseudo_hdr_t * pseudoChecksum = (sr_tcp_pseudo_hdr_t *) tempCopy;
  sr_tcp_hdr_t* tcpHeader = getTCPHeader(packet);
  uint16_t checkSum = 0;
  uint16_t verifySum = tcpHeader->checkSum;
  tcpHeader->checkSum = 0;
  memcpy(tempCopy + sizeof(sr_tcp_pseudo_hdr_t), tcpHeader, tcpLen);
  pseudoChecksum->src = iphdr->ip_src;
  pseudoChecksum->dst = iphdr->ip_dst;
  pseudoChecksum->zeroes = 0;
  pseudoChecksum->p = ip_protocol_tcp;
  pseudoChecksum->tcpLen = htons(tcpLen);
  checkSum = cksum(tempCopy, sizeof(sr_tcp_pseudo_hdr_t) + tcpLen);
  valid = (verifySum == checkSum) ? true : false;
  free(tempCopy);
  return valid;
}
sr_nat_mapping_t* processFirstConnection(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface,
  sr_nat_mapping_t* mapping, sr_tcp_hdr_t* tcpHeader) {
  if(mapping == NULL) {
    pthread_mutex_lock(&(sr->nat->lock));
    mapping = malloc(sizeof(sr_nat_mapping_t));
    sr_nat_connection_t* currConnection = malloc(sizeof(sr_nat_connection_t));
    sr_nat_mapping_t* currMapping = natCreateMapping(sr->nat, getIPHeader(packet)->ip_src, tcpHeader->s_port, nat_mapping_tcp);
    currConnection->connectionState = outBoundSyn;
    currConnection->lastAccessed = time(NULL);
    currConnection->inBoundSyn = NULL;
    currConnection->ip = getIPHeader(packet)->ip_dst;
    currConnection->port = tcpHeader->d_port;

    currConnection->next = currMapping->conns;
    currMapping->conns = currConnection;
    memcpy(mapping, currMapping, sizeof(sr_nat_mapping_t));
    pthread_mutex_unlock(&(sr->nat->lock));
  }
  else {
    pthread_mutex_lock(&(sr->nat->lock));
    sr_nat_mapping_t* currMapping = natLookupInternal(sr->nat, getIPHeader(packet)->ip_src, tcpHeader->s_port, nat_mapping_tcp);
    sr_nat_connection_t* currConnection = findConnection(currMapping, getIPHeader(packet)->ip_dst, tcpHeader->d_port);
    if(currConnection == NULL) {
      currConnection = malloc(sizeof(sr_nat_connection_t));
      currConnection->connectionState = outBoundSyn;
      currConnection->lastAccessed = time(NULL);
      currConnection->inBoundSyn = NULL;
      currConnection->ip = getIPHeader(packet)->ip_dst;
      currConnection->port = tcpHeader->d_port;
      currConnection->next = currMapping->conns;
      currMapping->conns = currConnection;
    }
    else if(currConnection->connectionState == inBoundSynPending) {
      currConnection->connectionState = connected;
      if(currConnection->inBoundSyn) {
        free(currConnection->inBoundSyn);
      }
    }
    else if(currConnection->connectionState == timeWait) {
      currConnection->connectionState = outBoundSyn;
    }
    pthread_mutex_unlock(&(sr->nat->lock));
  }
  return mapping;
}

void processOutboundFinConnection(struct sr_instance* sr, uint8_t* packet, unsigned int len,
  char* interface, sr_tcp_hdr_t* tcpHeader) {
  pthread_mutex_lock(&(sr->nat->lock));
  sr_nat_mapping_t* currMapping = natLookupInternal(sr->nat, getIPHeader(packet)->ip_src, 
    tcpHeader->s_port, nat_mapping_tcp);
  sr_nat_connection_t* currConnection = findConnection(currMapping, getIPHeader(packet)->ip_dst,
    tcpHeader->d_port);
  if(currConnection) {
    currConnection->connectionState = timeWait;
  }
  pthread_mutex_unlock(&(sr->nat->lock));
}
void natInterpretTCPPacketForForwarding(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  sr_tcp_hdr_t* tcpHeader = getTCPHeader(packet);
  sr_nat_mapping_t* mapping = sr_nat_lookup_internal(sr->nat, getIPHeader(packet)->ip_src, tcpHeader->s_port, 
    nat_mapping_tcp);
  if(ntohs(tcpHeader->offset_cbits) & TCP_SYN) {
    mapping = processFirstConnection(sr, packet, len, interface, mapping, tcpHeader);
  } 
  else if (mapping == NULL) {
    return;
  }
  else if (ntohs(tcpHeader->offset_cbits) & TCP_FIN) {
    processOutboundFinConnection(sr, packet, len, interface, tcpHeader);
  }
  natProcessOutboundPacket(sr, packet, len, interface, mapping);
  if(mapping)  {
    free(mapping);
  }
}

bool processInboundFirstConnection(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface,
  sr_nat_mapping_t* mapping, sr_tcp_hdr_t* tcpHeader) {
  if(mapping == NULL) {
  /*TODO : Send Type 3 ICMP message back*/    
    return false;
  }
  else {
    pthread_mutex_lock(&(sr->nat->lock));
    sr_nat_mapping_t* currMapping = natLookupExternal(sr->nat, tcpHeader->d_port, nat_mapping_tcp);
    sr_nat_connection_t* currConnection = findConnection(currMapping, getIPHeader(packet)->ip_src, tcpHeader->s_port);
    if(currConnection == NULL) {
      currConnection = malloc(sizeof(sr_nat_connection_t));
      currConnection->connectionState = inBoundSynPending;
      currConnection->inBoundSyn = malloc(len);
      memcpy(currConnection->inBoundSyn, packet, len);
      currConnection->ip = getIPHeader(packet)->ip_src;
      currConnection->port = tcpHeader->s_port;
      currConnection->next = currMapping->conns;
      currMapping->conns = currConnection;
      pthread_mutex_unlock(&(sr->nat->lock));
    }
    else if (currConnection->connectionState == inBoundSynPending) {
      pthread_mutex_unlock(&(sr->nat->lock));
      return false;
    }
    else if(currConnection->connectionState == outBoundSyn) {
      currConnection->connectionState = connected;
      pthread_mutex_unlock(&(sr->nat->lock));
    }
    else {
      pthread_mutex_unlock(&(sr->nat->lock));
    }
  }
  return true;
}
void natInterpretTCPPacketInbound(struct sr_instance* sr, uint8_t* packet, unsigned int len,
  char* interface) {
  sr_tcp_hdr_t* tcpHeader = getTCPHeader(packet);
  sr_nat_mapping_t* mapping = sr_nat_lookup_external(sr->nat, tcpHeader->d_port, 
    nat_mapping_tcp);
  if(ntohs(tcpHeader->offset_cbits) & TCP_SYN) {
    if(!processInboundFirstConnection(sr, packet, len, interface, mapping, tcpHeader)) {
      return;
    }
  }
  else if (mapping == NULL) {
    /* TODO : Send ICMP unreachable message */
    return;
  }
  else if(ntohs(tcpHeader->offset_cbits) & TCP_FIN) {
    pthread_mutex_lock(&(sr->nat->lock));
    sr_nat_mapping_t* currMapping = natLookupExternal(sr->nat, tcpHeader->d_port, nat_mapping_tcp);
    sr_nat_connection_t* currConnection = findConnection(currMapping, getIPHeader(packet)->ip_src, tcpHeader->s_port);
    if(currConnection) {
      currConnection->connectionState = timeWait;
    }
    pthread_mutex_unlock(&(sr->nat->lock));
  }
  else {
    pthread_mutex_lock(&(sr->nat->lock));
    sr_nat_mapping_t* currMapping = natLookupExternal(sr->nat, tcpHeader->d_port, nat_mapping_tcp);
    sr_nat_connection_t* currConnection = findConnection(currMapping, getIPHeader(packet)->ip_src, tcpHeader->s_port);
    if(currConnection == NULL) {
      pthread_mutex_unlock(&(sr->nat->lock));
      return;
    }
    else {
      pthread_mutex_unlock(&(sr->nat->lock));
    }
  }
  natProcessInboundPacket(sr, packet, len, interface, mapping);
  if(mapping) {
    free(mapping);
  }
}
void natProcessTCPPacket(struct sr_instance* sr, uint8_t* packet, unsigned int len, char* interface) {
  if(TCPPacketValid(packet, len)) {
    printf("TCP Checksum is valid...\n");
    struct sr_if* currentInterface = sr_get_interface(sr, interface);
    int interfaceMatchFound = checkListOfInterfaces(sr, packet);
    int ipMatchFound = checkListOfIPs(sr, packet);
    if(getInternalInterface(sr)->ip == currentInterface->ip) {
      if(interfaceMatchFound == 1 && ipMatchFound == 1) {
       /* printf("Just simply interpreting for self..\n");*/
        interpretPacketForSelf(sr, packet, len);
      }
      else if(interfaceMatchFound == 1 && ipMatchFound == 0) {
        natInterpretTCPPacketForForwarding(sr, packet, len, interface);
      }
    }
    else {
      natInterpretTCPPacketInbound(sr, packet, len, interface);
    }
  }
}

void natProcessIPPacket(struct sr_instance * sr, uint8_t* packet, unsigned int len, char* interface) {
  sr_ip_hdr_t* iphdr = getIPHeader(packet);
  if(iphdr->ip_p == ip_protocol_tcp) {
    printf("Processing TCP...\n");
    natProcessTCPPacket(sr, packet, len, interface);
  }
  else if(iphdr->ip_p == ip_protocol_icmp) {
    printf("Processing ICMP... \n");
    natProcessICMPPacket(sr, packet, len, interface);
  }
}


